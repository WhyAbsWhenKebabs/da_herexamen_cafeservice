package main.java.restController;

import cafes.db.DBException;
import cafes.db.ValidationException;
import cafes.domain.Cafe;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.util.List;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;
import service.CafeFacade;

/**
 *
 * @author Tom
 */
@RequestScoped
@Path("/cafes")
public class CafeRestController {
    
    @Inject
    private CafeFacade cafeFacade;

    public CafeRestController() {
        
    }
    
    @GET
    @Produces("application/json")
    public Response getCafes(){
        try{
            ObjectMapper mapper = new ObjectMapper();
            List<Cafe> cafes = cafeFacade.getCafes();
            String jsonInString = mapper.writeValueAsString(cafes);
            return Response.status(200).entity(jsonInString).build();
        }catch (JsonProcessingException jsonEx) {
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(jsonEx.getMessage()).build();
        }
    }    
    
    @Path("/{cafeID}")
    @GET
    @Produces("application/json")
    public Response getCafe(@PathParam("cafeID") long cafeID){
        try{
            ObjectMapper mapper = new ObjectMapper();
            Cafe cafe = cafeFacade.getCafe(cafeID);
            String jsonInString = mapper.writeValueAsString(cafe);
            return Response.status(200).entity(jsonInString).build();
        }catch (JsonProcessingException jsonEx) {
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(jsonEx.getMessage()).build();
        }catch (DBException | ValidationException ex){
            return Response.status(Response.Status.BAD_REQUEST).entity(ex.getMessage()).build();
        }
    }
    
    @Path("/add")
    @POST
    @Consumes("application/json")
    public Response addCafe(Cafe cafe){
        try{
            this.cafeFacade.addCafe(cafe);
            return Response.ok("new cafe created: " + cafe).build();
        }catch (DBException | ValidationException ex){
            return Response.status(Response.Status.BAD_REQUEST).entity(ex.getMessage()).build();
        }
    }
    
    @Path("/delete/{cafeID}")
    @DELETE
    public Response deleteCafe(@PathParam("cafeID") long cafeID){
        try{
            this.cafeFacade.deleteCafe(cafeID);
            return Response.ok("cafe deleted: ").build();
        }catch (DBException | ValidationException ex){
            return Response.status(Response.Status.BAD_REQUEST).entity(ex.getMessage()).build();
        }
    }
    
    @Path("/update")
    @PUT
    @Consumes("application/json")
    public Response updateCafe(Cafe cafe){
        try{
            this.cafeFacade.updateCafe(cafe);
            return Response.ok("cafe updated: " + cafe).build();
        }catch (DBException | ValidationException ex){
            return Response.status(Response.Status.BAD_REQUEST).entity(ex.getMessage()).build();
        }
    }
    
    @Path("/matchToView/{footballMatchID}")
    @GET
    public Response listOfCafesSendingOutSpecificFootballMatch(@PathParam("footballMatchID") long footballMatchID){
        try{
            ObjectMapper mapper = new ObjectMapper();
            List<Cafe> cafes = cafeFacade.listOfCafesSendingOutSpecificFootballMatch(footballMatchID);
            String jsonInString = mapper.writeValueAsString(cafes);
            return Response.status(200).entity(jsonInString).build();
        }catch (JsonProcessingException jsonEx) {
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(jsonEx.getMessage()).build();
        }catch (ValidationException ex){
            return Response.status(Response.Status.BAD_REQUEST).entity(ex.getMessage()).build();
        }
    }    
    
}
