package main.java.servletController;

import cafes.db.DBException;
import cafes.db.ValidationException;
import cafes.domain.Cafe;
import cafes.domain.restService.footballMatch.FootballMatch;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.sun.media.jfxmedia.logging.Logger;
import java.io.IOException;
import java.util.List;
import javax.ejb.EJB;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import service.CafeFacade;

/**
 *
 * @author Tom
 */
@WebServlet(urlPatterns = {"/controller"})
public class CafeController extends HttpServlet {
    
    @EJB
    private CafeFacade cafeFacade;
    
    private String error;
    
    // server
    // private static final String FOOTBALLMATCH_SERVICE_PATH = "http://193.191.187.14:11198/FootballMatchService-0.0.1/rest/matches";
    
    // lokaal
    private static final String FOOTBALLMATCH_SERVICE_PATH = "http://localhost:8080/FootballMatchService/rest/matches";

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String action = request.getParameter("action");
        String destination = null;
        if(action != null){
            if(action.equals("addFootballMatch")){
                System.out.println("adding football match");
                String footballMatchID = request.getParameter("footballMatchID");
                String cafeID = request.getParameter("cafeID");
                System.out.println("parameters: " + footballMatchID + " " + cafeID);
                FootballMatch footballMatch = null;
                try{
                    long footballMatchIDLong = Long.parseLong(footballMatchID);
                    footballMatch = this.getFootballMatch(footballMatchIDLong);
                }catch(NumberFormatException n){
                    request.setAttribute("error", "no correct number was given");
                }
                if(footballMatch == null){
                    if(this.error != null && !this.error.isEmpty()){
                        // setting error msg
                        request.setAttribute("error", this.error);
                        this.error = null;
                    }
                }else{
                    try{
                        this.cafeFacade.addExtraMatchToNextMatchesToView(footballMatch, Long.parseLong(cafeID));
                    }catch(ValidationException | DBException ex){
                        request.setAttribute("error", ex.getMessage());
                    }
                }
            }else if(action.equals("moreInfoMatch")){
                System.out.println("receiving more information about match");
                String footballMatchID = request.getParameter("footballMatchID");
                FootballMatch footballMatch = this.getFootballMatch(Long.parseLong(footballMatchID));
                if(footballMatch == null){
                    if(this.error != null && !this.error.isEmpty()){
                        request.setAttribute("error", this.error);
                        this.error = null;
                    }
                }else{
                    request.setAttribute("footballMatch", this.getFootballMatch(Long.parseLong(footballMatchID)));
                    destination = "footballMatch.jsp";
                }                
            }
        }
        if(destination == null){
            // go to index.jsp
            destination = "index.jsp";
            List<Cafe> cafes= this.cafeFacade.getCafes();
            request.setAttribute("cafes", cafes);
        }        
        
        RequestDispatcher view = request.getRequestDispatcher(destination);
        view.forward(request, response);
    }
    
    private FootballMatch getFootballMatch(long footballMatchID) throws IOException{
        FootballMatch footballMatch = null;
        try{
            Client client = ClientBuilder.newClient();        
            WebTarget target = client.target(FOOTBALLMATCH_SERVICE_PATH + "/" + footballMatchID);
            Response response = target.request(MediaType.APPLICATION_JSON).get();
            String json = response.readEntity(String.class);
            System.out.println(json);
            if(response.getStatusInfo().equals(Response.Status.BAD_REQUEST)){
                // bad request .. Invalid data or data not found etc.
                this.error = json;
                return null;
            }
            response.close();            
            footballMatch = new ObjectMapper().readValue(json, FootballMatch.class);
        }catch(Exception ex){
            // error
            System.out.println("SERVER ERROR");
            System.out.println(ex.getMessage());
            ex.printStackTrace();        
        }
        return footballMatch;
    }
    
    

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
