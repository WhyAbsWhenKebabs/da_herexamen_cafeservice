<%-- 
    Document   : index
    Created on : 9-aug-2017, 11:42:55
    Author     : Tom
--%>

<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="sql" uri="http://java.sun.com/jsp/jstl/sql"%>
<%@ taglib prefix="x" uri="http://java.sun.com/jsp/jstl/xml"%>
<!DOCTYPE html>
<html>
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <meta charset="UTF-8">
        <link rel="stylesheet" type="text/css" href="<c:url value="/css/custom.css"/>">
        <title>Cafe Service</title>
    </head>
    <body>
        <h1>Cafe Service</h1>
        <br>
        <div>
            <c:if test="${error != null && !error.isEmpty()}">
                <p>${error}</p>
            </c:if>
        </div>
        <div>
            <c:forEach var="cafe" items="${cafes}">
                <section>
                    <p>${cafe}</p>
                    <p>sending out following matches: </p>
                    <ul>
                        <c:forEach var="footballMatch" items="${cafe.nextMatchesToWatch}">
                            <li>${footballMatch} <a href="controller?action=moreInfoMatch&footballMatchID=${footballMatch.footballMatchID}">more information ...</a></li>
                        </c:forEach>
                    </ul>
                    <form method="post" action="controller?action=addFootballMatch">
                           <label for="footballMatch">Add Football match to cafe (give football match ID)</label> 
                           <input type="number" name="footballMatchID" />
                           <input hidden name="cafeID" value="${cafe.cafeID}" />
                           <input type="submit" value="add"/>
                    </form>
                </section>
                <br>
                <br>
            </c:forEach>
        </div>
            
    </body>
</html>
