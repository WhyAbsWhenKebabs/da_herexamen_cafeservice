<%-- 
    Document   : index
    Created on : 9-aug-2017, 11:42:55
    Author     : Tom
--%>

<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="sql" uri="http://java.sun.com/jsp/jstl/sql"%>
<%@ taglib prefix="x" uri="http://java.sun.com/jsp/jstl/xml"%>
<!DOCTYPE html>
<html>
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <meta charset="UTF-8">
        <link rel="stylesheet" type="text/css" href="<c:url value="/css/custom.css"/>">
        <title>More information about football match</title>
    </head>
    <body>
        <h1>${footballMatch.homeTeam} - ${footballMatch.awayTeam}</h1>
        
        <p>Date: ${footballMatch.dateOfMatch}</p>
        <p>Odd: ${footballMatch.odd}</p>
        <p>Last confrontation: ${footballMatch.lastConfrontation}</p>
    </body>
</html>
